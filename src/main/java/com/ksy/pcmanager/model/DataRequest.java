package com.ksy.pcmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataRequest {
    private String computerName;
    private String managerName;
}
