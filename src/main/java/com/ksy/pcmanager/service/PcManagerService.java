package com.ksy.pcmanager.service;

import com.ksy.pcmanager.Entity.PcManager;
import com.ksy.pcmanager.repository.PcManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcManagerService {
    private final PcManagerRepository connectRepository;

    public void setPcManager(String comName, String managerName) {
        PcManager addData = new PcManager();
        addData.setComputerName(comName);
        addData.setManagerName(managerName);

        connectRepository.save(addData);
    }
}
