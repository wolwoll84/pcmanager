package com.ksy.pcmanager.repository;

import com.ksy.pcmanager.Entity.PcManager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagerRepository extends JpaRepository<PcManager, Long> {
}
