package com.ksy.pcmanager.controller;

import com.ksy.pcmanager.model.DataRequest;
import com.ksy.pcmanager.service.PcManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pcmanager")
public class PcManagerController {
    private final PcManagerService pcManagerService;

    @PostMapping("/data")
    public String setData(@RequestBody DataRequest request) {
        pcManagerService.setPcManager(request.getComputerName(), request.getManagerName());
        return "등록되었습니다.";
    }
}
